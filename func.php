<?php

use Longman\TelegramBot\Request;
include_once 'Db.php';

define('TIME', time());

function fn_resend_to_admin($update) {
    if($update->message['chat']['id'] != ADMIN_CHAT_ID) {
        Request::forwardMessage([
            'chat_id' => ADMIN_CHAT_ID,
            'from_chat_id' => $update->message['chat']['id'],
            'message_id' => $update->message['message_id']
        ]);

        if (isset($update->message['sticker'])) {
            Request::sendMessage([
                'chat_id' => ADMIN_CHAT_ID,
                'text' => sprintf(
                    'Стикер от %d %s %s @%s',
                    $update->message['from']['id'],
                    $update->message['from']['first_name'],
                    isset($update->message['from']['last_name']) ? $update->message['from']['last_name'] : '',
                    isset($update->message['from']['username']) ? $update->message['from']['username'] : ''
                )
            ]);
        }
    }
}

function fn_response_from_admin($update) {
    if($update->message['from']['id'] == ADMIN_CHAT_ID && !empty($update->message['reply_to_message']['forward_from'])) {
        $reply['chat_id'] = $update->message['reply_to_message']['forward_from']['id'];
        if(isset($update->message['text'])) {
            $reply['text'] = $update->message['text'];
            Request::sendMessage($reply);
        }elseif (isset($update->message['sticker'])) {
            $reply['sticker'] = $update->message['sticker']['file_id'];
            Request::sendSticker($reply);
        }elseif (isset($update->message['document'])) {
            $reply['document'] = $update->message['document']['file_id'];
            Request::sendDocument($reply);
        }elseif (isset($update->message['photo'][0])) {
            $reply['photo'] = $update->message['photo'][0]['file_id'];
            if(isset($update->message['caption'])) {
                $reply['caption'] = $update->message['caption'];
            }
            Request::sendDocument($reply);
        }elseif (isset($update->message['voice'])) {
            $reply['voice'] = $update->message['voice']['file_id'];
            Request::sendVoice($reply);
        }
        unset($reply);
        return true;
    }
    return false;
}

function fn_report($update, $command, $argv, $argc) {

    $sql = '
SELECT
 DATE_FORMAT(FROM_UNIXTIME(t1.`timestamp`), \'%d-%m-%Y\') AS work_date,
 DATE_FORMAT(FROM_UNIXTIME(t1.`timestamp`), \'%H:%i:00\') AS come,
 DATE_FORMAT(FROM_UNIXTIME(t2.`timestamp`), \'%H:%i:00\') AS \'leave\'
FROM `reports` AS t1
LEFT JOIN `reports` AS t2 ON DATE_FORMAT(FROM_UNIXTIME(t1.`timestamp`), \'%d-%m-%Y\') LIKE DATE_FORMAT(FROM_UNIXTIME(t2.`timestamp`), \'%d-%m-%Y\') AND t2.`type` = \'L\' AND t1.user_id = t2.user_id
WHERE 1
 AND t1.`type` = \'C\'
 AND DATE_FORMAT(FROM_UNIXTIME(t1.`timestamp`), \'%m-%Y\') LIKE ?
 AND t1.user_id = ?
';
    $time = TIME;

    if($command == 'last_month') {
        $time = strtotime('first day of previous month');
    }

    $res = Db::getRowArray($sql, [
        date('m-Y', $time),
        $update->message['from']['id']
    ]);

    $data_days = [];
    foreach ($res as $row) {
        if(!empty($row['work_date'])) {
            $data_days[$row['work_date']] = [$row['come'], $row['leave']];
        }
    }
    print_r([
        'data_days' => $data_days,
        date('m-Y', $time),
        $update->message['from']['id']
    ]);

    $res = Db::getRowArray(
        'SELECT FROM_UNIXTIME(`date`, \'%d-%m-%Y\') AS normal_date, `comment` FROM holidays WHERE `date` >= ?',
        [strtotime('01' . date('-m-Y', $time))]
    );

    $holidays = [];
    foreach ($res as $row) {
        $holidays[$row['normal_date']] = $row['comment'];
    }

    $color = isset($argv[1]) ? $argv[1] : '';
    preg_match('/#([a-f0-9A-F]{3}){1,2}\b/', $color, $matches);
    $color = isset($matches[0]) ? $matches[0] : '#80a1c1';
    $color = str_replace('#', '', $color);
    $color = mb_strtolower($color);

    $report = new CreateReport(
        $update->message['from']['id'], $data_days, $time, $holidays, $color
    );

    Request::sendDocument([
        'chat_id' => $update->message['chat']['id'],
        'document' => Request::encodeFile($report->getPath())
    ]);
}

function fn_holiday($update, $command, $argv, $argc) {

    if($update->message['from']['id'] != ADMIN_CHAT_ID) return;

    if(!empty($argv[1])) {

        $comment = empty($argv[3]) ? '' : $argv[3];

        if(!empty($argv[2])) {
            $period = new DatePeriod(
                new DateTime($argv[1]),
                new DateInterval('P1D'),
                new DateTime($argv[2])
            );

            $data = [];
            $str = [];
            /* @var $day DateTime */
            foreach ($period as $day) {
                $data[] = $day->getTimestamp();
                $data[] = $comment;
                $str[] = '(?,?)';
            }
            $str = implode(',', $str);

        }else {
            $data = [
                strtotime($argv[1]),
                $comment
            ];
            $str = '(?,?)';
        }

        Db::query('INSERT INTO holidays (`date`,`comment`) VALUES '. $str, $data);
    }
}

function fn_hello($update, $command, $argv, $argc) {

    $types = [
        'hi' => 'C',
        'hello' => 'C',
        'привет' => 'C',

        'bb' => 'L',
        'goodbye' => 'L',
        'пока' => 'L'
    ];

    if(empty($argv[0]) || !isset($types[$command])) {
        return;
    }

    $date = empty($argv[1])
        ? $update->message['date']
        : strtotime(date('Y-m-d ', $update->message['date']) . $argv[1]);

    $res = Db::getRow(
        'SELECT id FROM reports WHERE `type` = ? AND DATE_FORMAT(FROM_UNIXTIME(`timestamp`), \'%d-%m-%Y\') LIKE ? AND user_id = ?',
        [
            $types[$command],
            date('d-m-Y', $date),
            $update->message['from']['id']
        ]
    );

    print_r([
        $types[$command],
        date('d-m-Y', $date),
        $update->message['from']['id']
    ]);

    print_r($res);
    if (!empty($res['id'])) {
        echo "UPDATE\n";
        Db::query('UPDATE reports SET `timestamp` = ? WHERE id = ?', [$date, $res['id']]);
    } else {
        echo "INSERT\n";
        Db::query('INSERT INTO reports (user_id, `timestamp`, `type`) VALUES(?, ?, ?)', [
            $update->message['from']['id'],
            $date,
            $types[$command]
        ]);
    }
    Request::sendMessage([
        'chat_id' => $update->message['chat']['id'],
        'text' => $argv[0] . '! ' . date('Y-m-d H:i:s', $date)
    ]);
}

function fn_english_words($update, $command, $argv, $argc) {

    $res = Db::getRow('SELECT * FROM english_users_progress WHERE user_id = ?', [$update->message['from']['id']]);
    $new_progress = empty($res['word_id']);

    $res = Db::getRow('SELECT * FROM english_words WHERE id = ?', [!$new_progress ? ($res['word_id'] + 1) : 2]);

    if (!empty($res['id'])) {
        Request::sendMessage([
            'chat_id' => $update->message['chat']['id'],
            'text' => $res['word'] . ' - ' . $res['translate']
        ]);

        Db::query(
            !$new_progress
                ? 'UPDATE english_users_progress SET word_id = ? WHERE user_id = ?'
                : 'INSERT INTO english_users_progress (word_id, user_id) VALUES(?, ?)',
            [
                $res['id'],
                $update->message['from']['id']
            ]
        );
    } else {
        Request::sendMessage([
            'chat_id' => $update->message['chat']['id'],
            'text' => 'Слова закончились'
        ]);
    }
}

function fn_game($update, $command, $argv, $argc) {

    $params = [
        'connect' => 'csworld.kg:27960',
        'name' => !empty($update->message['from']['username']) ? $update->message['from']['username'] : 'noname',
        'password' => 211299
    ];
    $query = '';
    foreach ($params as $key => $item) {
        $query .= $key . '%20' . urlencode($item) . ';';
    }
    $query = substr($query, 0, mb_strlen($query) - 1);
    $url = 'http://www.quakejs.com/play?' . $query;


    Request::sendMessage([
        'chat_id' => $update->message['chat']['id'],
        'text' => $url
    ]);
}


function fn_notifications($update, $command, $argv, $argc) {

    $id = $update->message['from']['id'];
    $res = Db::getRow('SELECT notification_id FROM notifications WHERE user_id = ?', [$id]);

    if(!empty($res['notification_id'])) {
        Db::query('DELETE FROM notifications WHERE user_id = ?', [$id]);
        Request::sendMessage([
            'chat_id' => $update->message['chat']['id'],
            'text' => 'Notifications: OFF'
        ]);
    }else {
        Db::query('INSERT INTO notifications (user_id) VALUES(?)', [$id]);
        Request::sendMessage([
            'chat_id' => $update->message['chat']['id'],
            'text' => 'Notifications: ON'
        ]);
    }
}

function fn_send_notifications() {

    $stickers = [
        'CAADAgADsgMAAp38IwABHByT5HuexssWBA',
        'CAADAgAD_QIAAp38IwABewrgE7vtQlQWBA',
        'CAADAgADrwMAAp38IwABt5jYQ7_onloWBA',
        'CAADAgAD-QIAAp38IwAByXikI2hoEjkWBA',
        'CAADAgADMwIAAp38IwABAQmouxDnYB0WBA',
        'CAADAgADXgEAAp38IwABRtZ8dsLgEowWBA',
        'CAADBAADagADXSupAQsbG7vJr-NUFgQ',
        'CAADAgADvgAD8MPADlcXKOi4nawCFgQ',
        'CAADAgADOAAD8MPADlpYkvShhDUKFgQ',
        'CAADAgADrQAD8MPADrISv4fypGqoFgQ',
        'CAADAgADxwAD8MPADqdOWGrO1kagFgQ',
        'CAADAgADogAD8MPADsj7r6AsFim_FgQ',
        'CAADBQADlgMAAukKyAOT1rGMgdf88RYE',
        'CAADBQADowMAAukKyANuF7zcXpotjBYE',
        'CAADBQADpQMAAukKyANcaWgXsp1YPxYE',
        'CAADBQADqQMAAukKyANqmZZeaEc9pBYE',
        'CAADBQADqgMAAukKyAOMMrddotAFYRYE',
        'CAADBQAD1AMAAukKyAOCImjQvTRwtxYE',
        'CAADBQAD0wMAAukKyAMsRH9b_BLzvRYE',
        'CAADBQAD1wMAAukKyAMbsfpNgFj5RBYE',
        'CAADAgADmQQAAulVBRi8-VqIiMu2WBYE',
        'CAADAgADrAQAAulVBRg29lq2QYdA_BYE',
        'CAADAgADrwQAAulVBRhi4Lnp9qpZWRYE',
        'CAADBAADXAgAAno8FQdSlr-a90hulBYE',
        'CAADBAADYAgAAno8FQdjSfgxMSxkyBYE',
        'CAADBAADYQgAAno8FQf_B73hxbk3CxYE',
        'CAADAgAD4wwAAmHpagSqpvLQpl498hYE',
        'CAADAgADwwwAAmHpagQV68y_Yvj5wRYE',
        'CAADAgADzQwAAmHpagQRX6tQmKC4ShYE',
        'CAADBAADLQADmDVxAtmLKycYAVEYFgQ',
        'CAADBAADIwADmDVxAvqoeoFePeT8FgQ'
    ];
    $sticker = $stickers[random_int(0, count($stickers) - 1)];

    $res = Db::getRowArray('SELECT user_id FROM notifications');

    foreach ($res as $row) {
        Request::sendSticker([
            'chat_id' => $row['user_id'],
            'sticker' => $sticker
        ]);
    }
}

function fn_update_message($update, $hooks) {
    fn_resend_to_admin($update);
    if(fn_response_from_admin($update)) return;

    $text = isset($update->message['text']) ? $update->message['text'] : '';
    $text = str_replace('/', '', $text);

    if(empty($text)) return;

    $argv = array_filter(explode(' ', $text));
    $command = mb_strtolower($argv[0]);
    $argc = count($argv);

    if(isset($hooks[$command])) {
        call_user_func($hooks[$command], $update, $command, $argv, $argc);
    }
}
