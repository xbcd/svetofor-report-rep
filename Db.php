<?php

/**
 * Class Db
 *
 */
class Db
{
    /**
     * @var $pdo PDO
     */
    private static $pdo;
    private static $params;

    public static function init($params)
    {
        self::$params = $params;
    }

    public static function connect()
    {
        $params = self::$params;
        self::$pdo = new PDO("mysql:host=$params[host];dbname=$params[database]", $params['user'], $params['password']);
        self::$pdo->exec('set names utf8');
    }

    public static function disconnect()
    {
        self::$pdo = null;
    }


    public static function query($sql, $params = [])
    {
        $query = self::$pdo->prepare($sql);
        $query->execute($params);
        return $query;
    }

    public static function getRow($sql, $params = [])
    {
        $query = self::query($sql, $params);
        $res = $query->fetch(PDO::FETCH_ASSOC);

        return $res;
    }

    public static function getRowArray($sql, $params = [])
    {
        $query = self::query($sql, $params);
        $res = $query->fetchAll(PDO::FETCH_ASSOC);

        return $res;
    }
}