<?php


class CreateReport
{
    private $time;
    private $user_id;
    private $color;

    const FORMAT_DATE = '[hh]:mm:ss';

    public function __construct($user_id, $data_days, $time, $holidays, $color = '80a1c1')
    {
        $this->user_id = $user_id;
        $this->color = $color;
        $document = new \PHPExcel();

        $sheet = $document->setActiveSheetIndex(0); // Выбираем первый лист в документе

        $columnPosition = 0; // Начальная координата x
        $startLine = 1; // Начальная координата y

        $header = [
            'Дата',
            'План',
            'Время прихода',
            'Время ухода',
            'Обед',
            'Отработанные часы'
        ];
        $plan = self::float_of_day_from_time('9:00:00');

        $launch = self::float_of_day_from_time('1:00:00');

        //вычитает обед
        //$work_hours_of_day = '=D[line]-C[line]-E[line]';
        $work_hours_of_day = '=D[line]-C[line]';
        foreach ($header as $item) {
            $sheet->getStyleByColumnAndRow($columnPosition, $startLine)
                ->getFont()
                ->setBold(true);
            $sheet->setCellValueByColumnAndRow($columnPosition, $startLine, $item);
            $sheet->getStyleByColumnAndRow($columnPosition, $startLine)->getAlignment()->setHorizontal(
                PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getColumnDimensionByColumn($columnPosition)->setWidth(20);
            $columnPosition++;
        }
        $columnPosition = 0;
        $startLine++;

        $this->time = $time;
        $days = range(1, date('t', $time));

        foreach ($days as $day) {
            $normal_date = sprintf('%02d', $day) . date('-m-Y', $time);
            $cell_date = strtotime($normal_date);
            $day_of_week = date('N', $cell_date);
            //$cell_date_format = date('d.m.y', $cell_date);

            $sheet->getStyleByColumnAndRow($columnPosition, $startLine)
                ->getNumberFormat()
                ->setFormatCode('dd.mm.yy');

            //$sheet->setCellValueByColumnAndRow($columnPosition, $startLine, $cell_date_format);
            $sheet->setCellValueByColumnAndRow($columnPosition, $startLine, self::get_exceltime_from_unixtime($cell_date + 21600));
            $sheet->getStyleByColumnAndRow($columnPosition, $startLine)
                ->getFill()
                ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB($this->color);

            $work_day = ($day_of_week < 6 && !key_exists($normal_date, $holidays));
            if($work_day || !empty($data_days[$normal_date])) {
                if($work_day) {
                    $sheet->setCellValueByColumnAndRow($columnPosition + 1, $startLine, $plan);
                }

                if(!empty($data_days[$normal_date])) {
                    $sheet->setCellValueByColumnAndRow($columnPosition + 2, $startLine, $data_days[$normal_date][0]);
                    $sheet->setCellValueByColumnAndRow($columnPosition + 3, $startLine, $data_days[$normal_date][1]);
                }

                $sheet->setCellValueByColumnAndRow($columnPosition + 4, $startLine, $launch);
                $sheet->setCellValueByColumnAndRow($columnPosition + 5, $startLine, str_replace('[line]', $startLine, $work_hours_of_day));

                foreach (range(1,6) as $num) {
                    $sheet->getStyleByColumnAndRow($columnPosition + $num, $startLine)
                        ->getNumberFormat()
                        ->setFormatCode(self::FORMAT_DATE);
                }

            } else {
                $sheet->setCellValueByColumnAndRow($columnPosition + 2, $startLine, 'Выходной');
                $sheet->getStyleByColumnAndRow($columnPosition + 2, $startLine)->getAlignment()->setHorizontal(
                    PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $document->getActiveSheet()->mergeCellsByColumnAndRow($columnPosition + 2, $startLine, $columnPosition+4, $startLine);
                $sheet->getStyleByColumnAndRow($columnPosition + 2, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($this->color);
            }
            foreach (range(0,6) as $column) {
                $sheet->getStyleByColumnAndRow($column, $startLine)->getAlignment()->setHorizontal(
                    PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            $startLine++;
        }

        $tableEndLine = $startLine-1;
        $sheet->setCellValueByColumnAndRow($columnPosition, $startLine, 'ПЛАН НА МЕСЯЦ');
        $sheet->setCellValueByColumnAndRow($columnPosition + 2, $startLine, "=SUM(B2:B$tableEndLine)");
        $sheet->getStyleByColumnAndRow($columnPosition + 2, $startLine)
            ->getNumberFormat()
            ->setFormatCode(self::FORMAT_DATE);

        $sheet->setCellValueByColumnAndRow($columnPosition + 4, $startLine, 'разница');
        $startLine++;
        $sheet->setCellValueByColumnAndRow($columnPosition + 4, $startLine, '=C'.($tableEndLine+1).'-C'.($tableEndLine+3));
        $sheet->getStyleByColumnAndRow($columnPosition + 4, $startLine)
            ->getNumberFormat()
            ->setFormatCode(self::FORMAT_DATE);
        $startLine++;
        $sheet->setCellValueByColumnAndRow($columnPosition, $startLine, 'ФАКТ');
        $sheet->setCellValueByColumnAndRow($columnPosition + 2, $startLine, "=SUM(F2:F$tableEndLine)");
        $sheet->getStyleByColumnAndRow($columnPosition + 2, $startLine)
            ->getNumberFormat()
            ->setFormatCode(self::FORMAT_DATE);

        $document->getActiveSheet()->mergeCellsByColumnAndRow($columnPosition, $tableEndLine+1, $columnPosition+1, $tableEndLine+2);
        $document->getActiveSheet()->mergeCellsByColumnAndRow($columnPosition, $tableEndLine+3, $columnPosition+1, $tableEndLine+4);

        $document->getActiveSheet()->mergeCellsByColumnAndRow($columnPosition+2, $tableEndLine+1, $columnPosition+3, $tableEndLine+2);
        $document->getActiveSheet()->mergeCellsByColumnAndRow($columnPosition+2, $tableEndLine+3, $columnPosition+3, $tableEndLine+4);

        $document->getActiveSheet()->mergeCellsByColumnAndRow($columnPosition+4, $tableEndLine+1, $columnPosition+5, $tableEndLine+1);
        $document->getActiveSheet()->mergeCellsByColumnAndRow($columnPosition+4, $tableEndLine+2, $columnPosition+5, $tableEndLine+4);

        $objWriter = \PHPExcel_IOFactory::createWriter($document, 'Excel5');
        $objWriter->save($this->getPath());
    }

    public function getPath()
    {
        return sprintf('temp/%s_%s.xls', date('m.Y', $this->time), $this->user_id);
    }

    private function seconds_from_time($time) {
        list($h, $m, $s) = explode(':', $time);
        return ($h * 3600) + ($m * 60) + $s;
    }
    private function time_from_seconds($seconds) {
        $h = floor($seconds / 3600);
        $m = floor(($seconds % 3600) / 60);
        $s = $seconds - ($h * 3600) - ($m * 60);
        return sprintf('%02d:%02d:%02d', $h, $m, $s);
    }

    private function float_of_day_from_time($time)
    {
        return self::seconds_from_time($time)/86400;
    }

    private static function get_exceltime_from_unixtime($unixtime)
    {
        return 25569 + ($unixtime / 86400);
    }

    private static function get_unixtime_from_exceltime($excel_time)
    {
        return ($excel_time - 25569) * 86400;
    }

}