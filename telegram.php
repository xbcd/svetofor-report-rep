<?php

define('ADMIN_CHAT_ID', 160416698);

require './vendor/autoload.php';
require_once 'config.php';
require_once './CreateReport.php';
require_once './Db.php';
require_once './func.php';

$hooks = include('./hooks.php');

try {

    // Create Telegram API object
    $telegram = new Longman\TelegramBot\Telegram($_TELEGRAM_BOT['token'], $_TELEGRAM_BOT['username']);

    // Enable MySQL
    //$telegram->enableMySql($_SQL);
    $telegram->useGetUpdatesWithoutDatabase();
    Db::init($_SQL);


    while (true) {
        // Handle telegram getUpdates request
        $server_response = $telegram->handleGetUpdates();
        if ($server_response->isOk()) {
            $result = $server_response->getResult();

            Db::connect();
            foreach ($result as $update) {
                print_r($update);
                if(empty($update->message)) continue;
                fn_update_message($update, $hooks);
            }
            Db::disconnect();// Close pdo connection

            $update_count = count($result);
            if($update_count) {
                echo date('Y-m-d H:i:s') . ' - Processed ' . $update_count . ' updates' . PHP_EOL;
            }
        } else {
            echo date('Y-m-d H:i:s') . ' - Failed to fetch updates' . PHP_EOL;
            echo $server_response->printError();
        }
        sleep(1);
    }


} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    // log telegram errors
    echo $e->getMessage();
}