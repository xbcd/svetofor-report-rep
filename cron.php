<?php


use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Telegram;

define('ADMIN_CHAT_ID', 160416698);

require './vendor/autoload.php';
require_once 'config.php';
require_once './Db.php';
require_once './func.php';



try {

    // Create Telegram API object
    $telegram = new Longman\TelegramBot\Telegram($_TELEGRAM_BOT['token'], $_TELEGRAM_BOT['username']);

    // Enable MySQL
    //$telegram->enableMySql($_SQL);
    $telegram->useGetUpdatesWithoutDatabase();
    Db::init($_SQL);
    Db::connect();

    fn_send_notifications();

    Db::disconnect();

} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    // log telegram errors
    echo $e->getMessage();
}